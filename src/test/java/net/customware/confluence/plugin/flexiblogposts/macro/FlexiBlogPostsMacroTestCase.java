package net.customware.confluence.plugin.flexiblogposts.macro;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResult;
import com.atlassian.confluence.status.service.SystemInformationService;
import com.atlassian.confluence.status.service.systeminfo.ConfluenceInfo;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys;
import net.customware.confluence.plugin.flexiblogposts.util.search.BlogPostSearcher;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.jmock.core.Constraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FlexiBlogPostsMacroTestCase extends MockObjectTestCase
{
    private Mock mockSystemInformationService;

    private Mock mockBlogPostSearcher;

    private Mock mockVelocityHelperService;

    private FlexiBlogPostsMacro flexiBlogPostsMacro;

    private StringBuffer assertRan;

    private Map<String, String> macroParams;

    private Page pageToBeRendered;

    private ConfluenceInfo confluenceInfo;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockSystemInformationService = new Mock(SystemInformationService.class);
        mockBlogPostSearcher = new Mock(BlogPostSearcher.class);
        mockVelocityHelperService = new Mock(VelocityHelperService.class);

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy());

        assertRan = new StringBuffer();

        macroParams = new HashMap<String, String>();
        pageToBeRendered = new Page();
        pageToBeRendered.setTitle("Test page");

        confluenceInfo = new ConfluenceInfo(null);

        mockVelocityHelperService.expects(atLeastOnce()).method("createDefaultVelocityContext").withNoArguments().will(returnValue(new HashMap<String, Object>()));
    }

    private String getFlexiBlogPostsServletUrl(final Map<String, Object> macroVelocityContext)
    {
        return new StringBuffer()
                .append("/plugins/servlet/flexiblogpostservlet?")
                .append("time=").append(macroVelocityContext.get("time"))
                .append("&maxResults=").append(macroVelocityContext.get("maxResults"))
                .append("&spaceKeys=").append(GeneralUtil.urlEncode(StringUtils.join((Collection) macroVelocityContext.get("spaceKeys"), ",")))
                .append("&labels=").append(GeneralUtil.urlEncode(StringUtils.join((Collection) macroVelocityContext.get("labels"), ",")))
                .append("&matchAllLabels=").append(macroVelocityContext.get("matchAllLabels"))
                .append("&columns=").append(StringUtils.join((Collection) macroVelocityContext.get("columns"), ","))
                .append("&compactMode=").append(macroVelocityContext.get("compactMode"))
                .toString();
    }

    public void testMacroRendersInline()
    {
        mockVelocityHelperService.reset();
        assertEquals(TokenType.BLOCK, flexiBlogPostsMacro.getTokenType(null, null, null));
    }

    public void testMacroHasNoBody()
    {
        mockVelocityHelperService.reset();
        assertFalse(flexiBlogPostsMacro.hasBody());
    }

    public void testMacroBodyNotRendered()
    {
        mockVelocityHelperService.reset();
        assertEquals(RenderMode.NO_RENDER, flexiBlogPostsMacro.getBodyRenderMode());
    }

    public void testBlogPostTitleDefaultsIfNoneIsSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(DEFAULT_TITLE, macroVelocityContext.get("title"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testBlogPostTitleRespectsTitleParam() throws MacroException
    {
        final String title = "Foobar";
        
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(title, macroVelocityContext.get("title"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("title", title);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testDurationRespectsTimeParam() throws MacroException
    {
        final String time = "1w";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(time, macroVelocityContext.get("time"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("time", time);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetMaxBlogPostsDefaultsWhenSpecifiedAsZero() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(BlogPostSearcher.DEFAULT_MAX_RESULT, ((Integer) macroVelocityContext.get("maxResults")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("0", "0");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetMaxBlogPostsDefaultsWhenSpecifiedSpecifiedAsValueLesserThanZero() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(BlogPostSearcher.DEFAULT_MAX_RESULT, ((Integer) macroVelocityContext.get("maxResults")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("0", "-1");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetMaxBlogPostsRespectsZerothParam() throws MacroException
    {
        final String maxResults = "1000";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(new Integer(maxResults), macroVelocityContext.get("maxResults"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("maxResults", maxResults);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testSpaceKeysResultOfCombinedSpaceAndSpacesParams() throws MacroException
    {
        final String spaceKey = "a";
        final String spaceKeys = "a, b, c";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final String spaceKeysCombined = StringUtils.join(
                        Arrays.asList(spaceKey, spaceKeys), ","
                );

                assertEquals(new HashSet<String>(Arrays.asList(StringUtils.split(spaceKeysCombined, ", "))), macroVelocityContext.get("spaceKeys"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("space", spaceKey);
        macroParams.put("spaces", spaceKeys);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testLabelsResultOfCombinedLabelAndLabelsParams() throws MacroException
    {
        final String label = "a";
        final String labels = "a, b, c";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final String labelsCombined = StringUtils.join(
                        Arrays.asList(label, labels), ","
                );

                assertEquals(new HashSet<String>(Arrays.asList(StringUtils.split(labelsCombined, ", "))), macroVelocityContext.get("labels"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("label", label);
        macroParams.put("labels", labels);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testMatchAllLabelsDefaultsToFalse() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertFalse((Boolean) macroVelocityContext.get("matchAllLabels"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testMatchAllLabelsIsTrueWhenMatchLabelsParamSetToAll() throws MacroException
    {
        final String matchAllLabels = "all";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertTrue((Boolean) macroVelocityContext.get("matchAllLabels"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("match-labels", matchAllLabels);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnsDefaultsToAllColumns() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(
                        new HashSet<String>(Arrays.asList("title", "author", "date-posted", "labels")),
                        macroVelocityContext.get("columns")
                );
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };
        
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnsOnlyRetainsValidColumnNames() throws MacroException
    {
        final String columns = "title, author, invalid1, invalid2";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(
                        new HashSet<String>(Arrays.asList("title", "author")), 
                        macroVelocityContext.get("columns")
                );
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("columns", columns);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetResultsPerPageDefaultsWhenZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(DEFAULT_RESULTS_PER_PAGE, ((Integer) macroVelocityContext.get("resultsPerPage")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("resultsPerPage", "0");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetResultsPerPageDefaultsWhenLesserThanZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(DEFAULT_RESULTS_PER_PAGE, ((Integer) macroVelocityContext.get("resultsPerPage")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("resultsPerPage", "-1");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetResultsPerPageRespectsResultsPerPageParam() throws MacroException
    {
        final String resultsPerPage = "1000";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(new Integer(resultsPerPage), macroVelocityContext.get("resultsPerPage"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("resultsPerPage", resultsPerPage);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetWidthDefaultsWhenZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(DEFAULT_WIDTH, ((Integer) macroVelocityContext.get("width")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("width", "0");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetWidthDefaultsWhenLesserThanZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(DEFAULT_WIDTH, ((Integer) macroVelocityContext.get("width")).intValue());
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("width", "-1");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetWidthRespectsWidthParam() throws MacroException
    {
        final String width = "1000";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(new Integer(width), macroVelocityContext.get("width"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("width", width);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetHeightReturnsNullWhenZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertNull(macroVelocityContext.get("height"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("height", "0");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetHeightReturnsNullWhenLesserThanZeroSpecified() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertNull(macroVelocityContext.get("height"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("height", "-1");
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testGetHeightRespectsHeightParam() throws MacroException
    {
        final String height = "1000";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(new Integer(height), macroVelocityContext.get("height"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("height", height);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnWidthsDefaults() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final List<Integer> expectedColumnWidths = new ArrayList<Integer>();

                for (final Map.Entry<String, Integer> entry : FlexiBlogPostsMacro.DEFAULT_COLUMN_WIDTHS.entrySet())
                    expectedColumnWidths.add(entry.getValue());


                assertEquals(expectedColumnWidths, macroVelocityContext.get("columnWidths"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };
        
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnWidthsRespectUserSpecifiedColumnWidths() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final List<Integer> expectedColumnWidths = Arrays.asList(1, 1, 1, 1);

                assertEquals(expectedColumnWidths, macroVelocityContext.get("columnWidths"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("columnWidths", "1,1,1,1");

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnWidthUseDefaultIfWidthSpecifiedByUserIsInvalid() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final List<Integer> expectedColumnWidths = Arrays.asList(FlexiBlogPostsMacro.DEFAULT_COLUMN_WIDTHS.get(BlogPostsColumnKeys.COLUMN_TITLE), 1, 1, 1);

                assertEquals(expectedColumnWidths, macroVelocityContext.get("columnWidths"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("columnWidths", "invalid,1,1,1");

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testColumnWidthUseDefaultForColumnsWhichTheirWidthsNotSpecifiedByUser() throws MacroException
    {
        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                final List<Integer> expectedColumnWidths = Arrays.asList(
                        1, 1,
                        FlexiBlogPostsMacro.DEFAULT_COLUMN_WIDTHS.get(BlogPostsColumnKeys.COLUMN_DATE_POSTED),
                        FlexiBlogPostsMacro.DEFAULT_COLUMN_WIDTHS.get(BlogPostsColumnKeys.COLUMN_LABELS)
                );

                assertEquals(expectedColumnWidths, macroVelocityContext.get("columnWidths"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("columnWidths", "1,1");

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testCompactModeTurnedOnWhenCompactParamIsSetToTrue() throws MacroException
    {
        final String compactMode = "true";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertTrue((Boolean) macroVelocityContext.get("compactMode"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("compact", compactMode);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testCompactModeTurnedOnWhenCompactParamIsSetToYes() throws MacroException
    {
        final String compactMode = "yes";

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
            {
                assertTrue((Boolean) macroVelocityContext.get("compactMode"));
                assertEquals(getFlexiBlogPostsServletUrl(macroVelocityContext), macroVelocityContext.get("flexiBlogPostServletUrl"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        macroParams.put("compact", compactMode);
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageToBeRendered.toPageContext());

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testBlogPostsSearchResultsInVelocityContextIfRenderedInPreviewMode() throws MacroException
    {
        final List<SearchResult> searchResult = new ArrayList<SearchResult>();
        final PageContext pageContext = pageToBeRendered.toPageContext();

        confluenceInfo.setBuildNumber(String.valueOf(1499));
        mockSystemInformationService.expects(atLeastOnce()).method("getConfluenceInfo").withNoArguments().will(returnValue(confluenceInfo));
        
        mockBlogPostSearcher.expects(once()).method("getBlogPosts").with(
                new Constraint[]{
                    eq(StringUtils.EMPTY),
                    eq(Collections.emptySet()),
                    eq(Collections.emptySet()),
                    eq(false),
                    eq(BlogPostSearcher.Sort.DATE_POSTED),
                    eq(true),
                    eq(BlogPostSearcher.DEFAULT_MAX_RESULT)
                }
        ).will(returnValue(searchResult));

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsStatically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(searchResult, macroVelocityContext.get("searchResults"));
                
                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        pageContext.setOutputType(RenderContext.PREVIEW);

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageContext);
        
        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testBlogPostsSearchResultsInVelocityContextIfRenderedInPdfExportMode() throws MacroException
    {
        final List<SearchResult> searchResult = new ArrayList<SearchResult>();
        final PageContext pageContext = pageToBeRendered.toPageContext();

        mockBlogPostSearcher.expects(once()).method("getBlogPosts").with(
                new Constraint[]{
                    eq(StringUtils.EMPTY),
                    eq(Collections.emptySet()),
                    eq(Collections.emptySet()),
                    eq(false),
                    eq(BlogPostSearcher.Sort.DATE_POSTED),
                    eq(true),
                    eq(BlogPostSearcher.DEFAULT_MAX_RESULT)
                }
        ).will(returnValue(searchResult));

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsStatically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(searchResult, macroVelocityContext.get("searchResults"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        pageContext.setOutputType(RenderContext.PDF);

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageContext);

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }

    public void testBlogPostsSearchResultsInVelocityContextIfRenderedInWordExportMode() throws MacroException
    {
        final List<SearchResult> searchResult = new ArrayList<SearchResult>();
        final PageContext pageContext = pageToBeRendered.toPageContext();

        mockBlogPostSearcher.expects(once()).method("getBlogPosts").with(
                new Constraint[]{
                    eq(StringUtils.EMPTY),
                    eq(Collections.emptySet()),
                    eq(Collections.emptySet()),
                    eq(false),
                    eq(BlogPostSearcher.Sort.DATE_POSTED),
                    eq(true),
                    eq(BlogPostSearcher.DEFAULT_MAX_RESULT)
                }
        ).will(returnValue(searchResult));

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsStatically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(searchResult, macroVelocityContext.get("searchResults"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        pageContext.setOutputType(RenderContext.WORD);

        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageContext);

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }



    public void testBlogPostsSearchResultsInVelocityContextOnlyContainsResultsInFirstPageInStaticRenderMode() throws MacroException
    {
        final List<SearchResult> searchResult = new ArrayList<SearchResult>();
        final PageContext pageContext = pageToBeRendered.toPageContext();

        searchResult.add(new LuceneSearchResult(new HashMap<String, String>()));
        searchResult.add(new LuceneSearchResult(new HashMap<String, String>()));

        mockBlogPostSearcher.expects(once()).method("getBlogPosts").with(
                new Constraint[]{
                    eq(StringUtils.EMPTY),
                    eq(Collections.emptySet()),
                    eq(Collections.emptySet()),
                    eq(false),
                    eq(BlogPostSearcher.Sort.DATE_POSTED),
                    eq(true),
                    eq(BlogPostSearcher.DEFAULT_MAX_RESULT)
                }
        ).will(returnValue(searchResult));

        flexiBlogPostsMacro = new FlexiBlogPostsMacro((SystemInformationService) mockSystemInformationService.proxy(), (BlogPostSearcher) mockBlogPostSearcher.proxy(), (VelocityHelperService) mockVelocityHelperService.proxy())
        {
            protected String renderBlogPostsStatically(Map<String, Object> macroVelocityContext)
            {
                assertEquals(searchResult.subList(0, 1), macroVelocityContext.get("searchResults"));

                assertRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        pageContext.setOutputType(RenderContext.PREVIEW);

        confluenceInfo.setBuildNumber(String.valueOf(1499));
        mockSystemInformationService.expects(atLeastOnce()).method("getConfluenceInfo").withNoArguments().will(returnValue(confluenceInfo));

        macroParams.put("resultsPerPage", "1"); /* 1 result per page */
        flexiBlogPostsMacro.execute(macroParams, StringUtils.EMPTY, pageContext);

        assertTrue(BooleanUtils.toBoolean(assertRan.toString()));
    }
}
