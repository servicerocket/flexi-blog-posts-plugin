package net.customware.confluence.plugin.flexiblogposts.util.search;

import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import junit.framework.Assert;
import org.apache.commons.lang.BooleanUtils;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.jmock.core.Constraint;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DefaultBlogPostSearcherTestCase extends MockObjectTestCase
{

    private DefaultBlogPostSearcher defaultBlogPostSearcher;

    private Mock mockSearchManager;

    private Mock mockSearchResults;

    private SearchResults searchResults;

    private StringBuffer assertsRan;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockSearchManager = new Mock(SearchManager.class);

        mockSearchResults = new Mock(SearchResults.class);
        searchResults = (SearchResults) mockSearchResults.proxy();

        assertsRan = new StringBuffer(); /* A place to keep track if required assertions are done */

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy());
    }

    public void testGetBlogPostsTimeSpanDefaultsToOneYearIfNotSpecified() throws InvalidSearchException
    {
        final Calendar oneYearAgo = Calendar.getInstance();

        oneYearAgo.add(Calendar.YEAR, -1);


        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

                Assert.assertEquals(
                        dateFormatter.format(oneYearAgo.getTime()),
                        dateFormatter.format(fromDate)
                );

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsTimeSpanRespectsDurationSpecified() throws InvalidSearchException
    {
        final Calendar oneYearAgo = Calendar.getInstance();

        oneYearAgo.add(Calendar.DAY_OF_YEAR, -712);


        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

                Assert.assertEquals(
                        dateFormatter.format(oneYearAgo.getTime()),
                        dateFormatter.format(fromDate)
                );

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                "712d",
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsTimeSpanDefaultsToOneYearAgoIfDurationSpecifiedIsInvalid() throws InvalidSearchException
    {
        final Calendar oneYearAgo = Calendar.getInstance();

        oneYearAgo.add(Calendar.YEAR, -1);


        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

                Assert.assertEquals(
                        dateFormatter.format(oneYearAgo.getTime()),
                        dateFormatter.format(fromDate)
                );

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                "xxx", /* Invalid duration */
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsSpaceKeysDefaultsToAllSpacesIfNotSpecified() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                Assert.assertEquals(1, spaceKeys.size());
                Assert.assertTrue(spaceKeys.contains(BlogPostSearcher.ALL_SPACES));

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsSpaceKeysExpandedToIncludeOnlyAllSpacesIfSpaceKeySpecifiedIsAliasAll() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                Assert.assertEquals(1, spaceKeys.size());
                Assert.assertTrue(spaceKeys.contains(BlogPostSearcher.ALL_SPACES));

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList(BlogPostSearcher.ALL_SPACES)),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsSpaceKeysContainsOnlyThoseSpecified() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                Assert.assertEquals(spaceKeys, new HashSet<String>(Arrays.asList("tst", "ds")));

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("ds", "tst")),
                new HashSet<String>(),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsLabelsDefaultToEmptySetIfNullSpecified() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                Assert.assertEquals(new HashSet<String>(), labelString);

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("TST")),
                null,
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);

        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsLabelsRespectsLabelsSpecified() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(isA(ISearch.class)).will(returnValue(searchResults));

        defaultBlogPostSearcher = new DefaultBlogPostSearcher((SearchManager) mockSearchManager.proxy())
        {
            protected SearchQuery constructSearchQuery(Date fromDate, Set<String> spaceKeys, Set<String> labelString, boolean matchAllLabels)
            {
                Assert.assertEquals(new HashSet<String>(Arrays.asList("bar", "foo")), labelString);

                assertsRan.append(Boolean.TRUE.toString());
                return null;
            }
        };

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(Arrays.asList("foo", "bar")),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                BlogPostSearcher.DEFAULT_MAX_RESULT);
        
        assertTrue(BooleanUtils.toBoolean(assertsRan.toString()));
    }

    public void testGetBlogPostsMaxResultsDefaultsToFifteenIfTheValueSpecifiedIsEqualsToZero() throws InvalidSearchException
    {

        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(
                new Constraint()
                {
                    public boolean eval(Object o)
                    {
                        return o instanceof ISearch
                                && ((ISearch) o).getResultFilter().getClass() == SubsetResultFilter.class
                                && ((SubsetResultFilter) ((ISearch) o).getResultFilter()).getMaxResults() == BlogPostSearcher.DEFAULT_MAX_RESULT;
                    }

                    public StringBuffer describeTo(StringBuffer stringBuffer)
                    {
                        return stringBuffer;
                    }
                }
        ).will(returnValue(searchResults));

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(Arrays.asList("foo", "bar")),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                0);
    }

    public void testGetBlogPostsMaxResultsDefaultsToFifteenIfTheValueSpecifiedIsLesserThanZero() throws InvalidSearchException
    {
        
        mockSearchResults.expects(once()).method("getAll").withNoArguments().will(returnValue(Collections.emptyList()));

        mockSearchManager.expects(once()).method("search").with(
                new Constraint()
                {
                    public boolean eval(Object o)
                    {
                        return o instanceof ISearch
                                && ((ISearch) o).getResultFilter().getClass() == SubsetResultFilter.class
                                && ((SubsetResultFilter) ((ISearch) o).getResultFilter()).getMaxResults() == BlogPostSearcher.DEFAULT_MAX_RESULT;
                    }

                    public StringBuffer describeTo(StringBuffer stringBuffer)
                    {
                        return stringBuffer;
                    }
                }
        ).will(returnValue(searchResults));

        defaultBlogPostSearcher.getBlogPosts(
                null,
                new HashSet<String>(Arrays.asList("TST")),
                new HashSet<String>(Arrays.asList("foo", "bar")),
                false,
                BlogPostSearcher.Sort.AUTHOR,
                true,
                -1);
    }
}
