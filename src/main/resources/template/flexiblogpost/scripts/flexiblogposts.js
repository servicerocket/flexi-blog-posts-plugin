jQuery(function($)
{
    var flexiBlogPosts = {
        getParamsFromFieldSet : function(fieldset)
        {
            var params = {};
            fieldset.children("input").each(function ()
            {
                params[$(this).attr('name')] = $(this).attr('value');
            });
            return params;
        },

        getScrollbarWidth : function()
        {
            var scrollbarWidth = 0;

            if ( $.browser.msie ) {
				var $textarea1 = $('<textarea cols="10" rows="2"></textarea>')
						.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo('body'),
					$textarea2 = $('<textarea cols="10" rows="2" style="overflow: hidden;"></textarea>')
						.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo('body');
				scrollbarWidth = $textarea1.width() - $textarea2.width();
				$textarea1.add($textarea2).remove();
			} else {
				var $div = $('<div />')
					.css({ width: 100, height: 100, overflow: 'auto', position: 'absolute', top: -1000, left: -1000 })
					.prependTo('body').append('<div />').find('div')
						.css({ width: '100%', height: 200 });
				scrollbarWidth = 100 - $div.width();
				$div.parent().remove();
			}

            return scrollbarWidth;
        }
    };

    $("div.flexi-blog-posts").each(function ()
    {
        var container = $(this);
        var fieldset = container.children("fieldset");
        var fieldsetParams = flexiBlogPosts.getParamsFromFieldSet(fieldset);
        var columns = [];
        var columnSize = parseFloat(fieldsetParams['columnSize']);
        var gridWidth = parseFloat(fieldsetParams['width']);
        var gridWidthWithoutScrollbarWidth = gridWidth - flexiBlogPosts.getScrollbarWidth() - (columnSize * 10 + 5);

        container.append("<table class=\"flexi-blog-posts-table\" style=\"display:none\"></table>");

        fieldset.children(".flexi-blog-post-columns").each(function (i)
        {
            var columnWidthInputName = this.value + '-width';
            var columnWidth = parseFloat(fieldsetParams[columnWidthInputName]);

            columns[i] = { display : this.name, name: this.value, sortable : true, align: 'left' };
            columns[i].width = Math.round((columnWidth / 100) * gridWidthWithoutScrollbarWidth);
        });

        container.children("table").flexigrid({
            url: fieldsetParams['flexi-blog-posts-servlet'],
            method: 'GET',
            dataType: 'json',
            colModel: columns,
            sortname: 'date-posted',
            sortorder: 'desc',
            usepager: true,
            title: fieldsetParams['title'],
            useRp: false,
            rp: parseInt(fieldsetParams['resultsPerPage']),
            showTableToggleBtn: true,
            width: gridWidth,
            height: (function() {
                return (fieldsetParams['height']) ? parseInt(fieldsetParams['height']) : 256;
            })(),
            onSuccess : function() {
                if (!$("input[name='height']", container).length) {
                    var blogPostTable = $("table.flexi-blog-posts-table", container).get(0);
                    var flexigrid = blogPostTable.grid;
                    var newHeight = blogPostTable.clientHeight + $(".hDiv", container).get(0).clientHeight;

                    $(".bDiv").css("height", newHeight + "px");

                    flexigrid.options.height = newHeight;
                    flexigrid.fixHeight(newHeight);
                }
            },
            onError : function(data)
            {
                container.append("<textarea class='flexi-blog-posts-error-log' style='overflow: scroll;' rows='5'></textarea>");
                container.children("textarea.flexi-blog-posts-error-log").each(
                        function()
                        {
                            var log = data.responseXML ? data.responseXML : data.responseText;
                            var textArea = $(this);

                            textArea.text(log);
                            textArea.css("width", '' + gridWidth + 'px');
                        }
                );
            }
        });
    });
});