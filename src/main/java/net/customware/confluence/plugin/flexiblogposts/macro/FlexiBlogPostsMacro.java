package net.customware.confluence.plugin.flexiblogposts.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.status.service.SystemInformationService;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import net.customware.confluence.plugin.flexiblogposts.util.search.BlogPostSearcher;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.ALL_COLUMNS;
import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.COLUMN_AUTHOR;
import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.COLUMN_DATE_POSTED;
import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.COLUMN_LABELS;
import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.COLUMN_TITLE;

public class FlexiBlogPostsMacro extends BaseMacro implements Macro
{
    static final Logger logger = LoggerFactory.getLogger(FlexiBlogPostsMacro.class);

    static final int DEFAULT_RESULTS_PER_PAGE = 15;

    static final int DEFAULT_WIDTH = 512;

    static final String DEFAULT_TITLE = StringUtils.EMPTY;

    static final Map<String, Integer> DEFAULT_COLUMN_WIDTHS = Collections.unmodifiableMap(new LinkedHashMap<String, Integer>()
    {
        {
            /* Widths in pct */
            put(COLUMN_TITLE, 36);
            put(COLUMN_AUTHOR, 21);
            put(COLUMN_DATE_POSTED, 14);
            put(COLUMN_LABELS, 29);
        }
    });

    private final SystemInformationService systemInformationService;

    private final BlogPostSearcher blogPostSearcher;

    private final VelocityHelperService velocityHelperService;

    public FlexiBlogPostsMacro(SystemInformationService systemInformationService, BlogPostSearcher blogPostSearcher, VelocityHelperService velocityHelperService)
    {
        this.systemInformationService = systemInformationService;
        this.blogPostSearcher = blogPostSearcher;
        this.velocityHelperService = velocityHelperService;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER; /* Irrelevant, since we don't have a body */
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException macroError)
        {
            throw new MacroException(macroError);
        }
    }

    public String execute(final Map<String, String> macroParams,
                          final String macroBody,
                          final ConversionContext conversionContext) throws MacroExecutionException 
    {
        final String title = getTitle(macroParams);
        final String duration = getDuration(macroParams);
        final int maxSearchResults = getMaxSearchResults(macroParams);
        final Set<String> spaceKeys = getSpaceKeys(macroParams);
        final Set<String> labels = getLabels(macroParams);
        final boolean matchAllLabels = getMatchAllLabels(macroParams);
        final Set<String> columns = getColumns(macroParams);
        final int resultsPerPage = getResultsPerPage(macroParams);
        final int width = getWidth(macroParams);
        final Integer height = getHeight(macroParams);
        final boolean compactMode = isCompactMode(macroParams);
        final List<Integer> columnWidths = getColumnsWidths(macroParams, columns);

        final Map<String, Object> macroVelocityContext = velocityHelperService.createDefaultVelocityContext();

        macroVelocityContext.put("title", title);
        macroVelocityContext.put("time", duration);
        macroVelocityContext.put("maxResults", maxSearchResults);
        macroVelocityContext.put("spaceKeys", spaceKeys);
        macroVelocityContext.put("labels", labels);
        macroVelocityContext.put("matchAllLabels", matchAllLabels);
        macroVelocityContext.put("columns", columns);
        macroVelocityContext.put("resultsPerPage", resultsPerPage);
        macroVelocityContext.put("width", width);
        if (null != height)
            macroVelocityContext.put("height", height);
        macroVelocityContext.put("compactMode", compactMode);
        macroVelocityContext.put("maxPossibleColumnCount", ALL_COLUMNS.size());
        macroVelocityContext.put("columnWidths", columnWidths);


        if (isStaticMode(conversionContext))
        {
            try
            {
                final int subListStart = 0;
                final int subListEnd = subListStart + resultsPerPage;
                List<SearchResult> searchResults = blogPostSearcher.getBlogPosts(
                        duration,
                        spaceKeys,
                        labels,
                        matchAllLabels,
                        BlogPostSearcher.Sort.DATE_POSTED,
                        true,
                        maxSearchResults
                );

                if (subListEnd <= searchResults.size())
                    searchResults = searchResults.subList(subListStart, subListEnd);

                macroVelocityContext.put("searchResults", searchResults);

                return renderBlogPostsStatically(macroVelocityContext);
            }
            catch (final InvalidSearchException ise)
            {
                throw new MacroExecutionException(ise);
            }
        }
        else
        {
            macroVelocityContext.put(
                    "flexiBlogPostServletUrl",
                    constructFlexiBlogPostUrl(duration, maxSearchResults, spaceKeys, labels, matchAllLabels, columns, compactMode));

            return renderBlogPostsDynamically(macroVelocityContext);
        }
    }

    protected String renderBlogPostsStatically(Map<String, Object> macroVelocityContext)
    {
        return velocityHelperService.getRenderedTemplate("template/flexiblogpost/flexiblogposts-static.vm", macroVelocityContext);
    }

    protected String renderBlogPostsDynamically(Map<String, Object> macroVelocityContext)
    {
        return velocityHelperService.getRenderedTemplate("template/flexiblogpost/flexiblogposts-dynamic.vm", macroVelocityContext);
    }

    protected String constructFlexiBlogPostUrl(
            final String time,
            final int maxResults,
            final Set<String> spaceKeys,
            final Set<String> labels,
            final boolean matchAllLabels,
            final Set<String> columns,
            final boolean compactMode)
    {
        return new StringBuffer()
                .append("/plugins/servlet/flexiblogpostservlet?")
                .append("time=").append(time)
                .append("&maxResults=").append(maxResults)
                .append("&spaceKeys=").append(GeneralUtil.urlEncode(StringUtils.join(spaceKeys, ",")))
                .append("&labels=").append(GeneralUtil.urlEncode(StringUtils.join(labels, ",")))
                .append("&matchAllLabels=").append(matchAllLabels)
                .append("&columns=").append(StringUtils.join(columns, ","))
                .append("&compactMode=").append(compactMode)
                .toString();
    }

    private String getTitle(final Map<String, String> macroParams)
    {
        return StringUtils.defaultString(macroParams.get("title"), DEFAULT_TITLE);
    }

    private int getMaxSearchResults(final Map<String, String> macroParams)
    {
        final String maxResultsString = StringUtils.trim(StringUtils.defaultString(macroParams.get("maxResults")));

        try
        {
            final int maxResults = Integer.parseInt(maxResultsString);
            if (maxResults <= 0)
                throw new NumberFormatException("Max result lesser than 0");

            return maxResults;
        }
        catch (final NumberFormatException nfe)
        {
            logger.debug("Invalid max results: " + maxResultsString + ". Reverting to the default of " + BlogPostSearcher.DEFAULT_MAX_RESULT, nfe);
            return BlogPostSearcher.DEFAULT_MAX_RESULT;
        }
    }

    private Set<String> getSpaceKeys(final Map<String, String> macroParams)
    {
        final String spaceKeysString = StringUtils.join(
                Arrays.asList(
                        StringUtils.defaultString(macroParams.get("space")),
                        StringUtils.defaultString(macroParams.get("spaces"))
                )
                , ","
        );

        return new HashSet<String>(Arrays.asList(StringUtils.split(spaceKeysString, ", ")));
    }

    private Set<String> getLabels(final Map<String, String> macroParams)
    {
        final String labelsString = StringUtils.join(
                Arrays.asList(
                        StringUtils.defaultString(macroParams.get("label")),
                        StringUtils.defaultString(macroParams.get("labels"))
                )
                , ","
        );

        return new HashSet<String>(Arrays.asList(StringUtils.split(labelsString, ", ")));
    }

    private boolean getMatchAllLabels(final Map<String, String> macroParams)
    {
        return StringUtils.equals("all", StringUtils.defaultString(macroParams.get("match-labels")));
    }

    private String getDuration(final Map<String, String> macroParams)
    {
        return StringUtils.defaultString(macroParams.get("time"));
    }

    private Set<String> getColumns(final Map<String, String> macroParams)
    {
        final String columnsString = StringUtils.defaultString(macroParams.get("columns"));

        if (StringUtils.isNotBlank(columnsString))
        {
            final Set<String> userColumns = new LinkedHashSet<String>(
                    Arrays.asList(
                            StringUtils.split(columnsString, ", ")
                    )
            );

            userColumns.retainAll(ALL_COLUMNS);
            return userColumns;
        }
        else
        {
            return ALL_COLUMNS;
        }
    }

    private int getResultsPerPage(final Map<String, String> macroParams)
    {
        final String resultsPerPageString = StringUtils.defaultString(StringUtils.trim(macroParams.get("resultsPerPage")));

        try
        {
            final int resultsPerPage = Integer.parseInt(resultsPerPageString);

            if (resultsPerPage <= 0)
                throw new NumberFormatException("Results per page lesser than or equals to 0: " + resultsPerPage);

            return resultsPerPage;
        }
        catch (final NumberFormatException nfe)
        {
            logger.debug("Unable to parse user specified results per page: " + resultsPerPageString, nfe);
            return DEFAULT_RESULTS_PER_PAGE;
        }
    }

    private int getWidth(final Map<String, String> macroParams)
    {
        final String widthString = StringUtils.defaultString(StringUtils.trim(macroParams.get("width")));

        try
        {
            final int width = Integer.parseInt(widthString);

            if (width <= 0)
                throw new NumberFormatException("Width lesser than or equals to 0: " + width);

            return width;
        }
        catch (final NumberFormatException nfe)
        {
            logger.debug("Unable to parse user specified width: " + widthString, nfe);
            return DEFAULT_WIDTH;
        }
    }

    private Integer getHeight(final Map<String, String> macroParams)
    {
        final String heightString = StringUtils.defaultString(StringUtils.trim(macroParams.get("height")));

        try
        {
            final int height = Integer.parseInt(heightString);

            if (height <= 0)
                throw new NumberFormatException("Height lesser than or equals to 0: " + height);

            return height;
        }
        catch (final NumberFormatException nfe)
        {
            logger.debug("Unable to parse user specified height: " + heightString, nfe);
            return null;
        }
    }

    private boolean isCompactMode(final Map<String, String> macroParams)
    {
        return BooleanUtils.toBoolean(StringUtils.defaultString(macroParams.get("compact")));
    }

    private List<Integer> getColumnsWidths(final Map<String, String> macroParams, final Set<String> columns)
    {
        final List<Integer> columnWidths = new ArrayList<Integer>(columns.size());
        final String[] columnWidthsString = StringUtils.split(StringUtils.trim(StringUtils.defaultString(macroParams.get("columnWidths"))), ", ");
        int counter = -1;

        for (final String column : columns)
        {
            if (++counter < columnWidthsString.length)
            {
                try
                {
                    columnWidths.add(Integer.parseInt(columnWidthsString[counter]));
                }
                catch (final NumberFormatException nfe)
                {
                    columnWidths.add(DEFAULT_COLUMN_WIDTHS.get(column));
                }
            }
            else
            {
                columnWidths.add(DEFAULT_COLUMN_WIDTHS.get(column));
            }
        }

        return columnWidths;
    }

    private boolean isStaticMode(final ConversionContext conversionContext)
    {
        final String outputType = conversionContext.getOutputType();

        return (StringUtils.equals(RenderContext.PREVIEW, outputType) && getBuildNumber() < 1500)
                || StringUtils.equals(RenderContext.PDF, outputType)
                || StringUtils.equals(RenderContext.WORD, outputType);
    }

    int getBuildNumber()
    {
        return Integer.parseInt(systemInformationService.getConfluenceInfo().getBuildNumber());
    }
}
