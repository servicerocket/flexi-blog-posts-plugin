package net.customware.confluence.plugin.flexiblogposts.servlet;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.user.User;
import net.customware.confluence.plugin.flexiblogposts.util.search.BlogPostSearcher;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static net.customware.confluence.plugin.flexiblogposts.util.BlogPostsColumnKeys.ALL_COLUMNS;

public class FlexiBlogPostsServlet extends HttpServlet
{
    private static final Logger logger = LoggerFactory.getLogger(FlexiBlogPostsServlet.class);

    private final BlogPostSearcher blogPostSearcher;

    private final FormatSettingsManager formatSettingsManager;

    private final UserAccessor userAccessor;

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    public FlexiBlogPostsServlet(BlogPostSearcher blogPostSearcher, FormatSettingsManager formatSettingsManager, UserAccessor userAccessor, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        this.blogPostSearcher = blogPostSearcher;
        this.formatSettingsManager = formatSettingsManager;
        this.userAccessor = userAccessor;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    protected DateFormatter getDateFormatter()
    {
        final ConfluenceUserPreferences confluenceUserPreferences =
                userAccessor.getConfluenceUserPreferences(
                        AuthenticatedUserThreadLocal.getUser()
                );

        return confluenceUserPreferences.getDateFormatter(formatSettingsManager, localeManager);
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            final String time = getTime(request);
            final int maxResults = getMaxResults(request);
            final Set<String> spaceKeys = getSpaceKeys(request);
            final Set<String> labels = getLabels(request);
            final boolean matchAllLabels = isMatchAllLabels(request);
            final int page = getPage(request);
            final int resultsPerPage = getResultsPerPage(request);
            final BlogPostSearcher.Sort sort = getSort(request);
            final boolean sortAscending = isSortAscending(request);
            final boolean compactMode = isCompactMode(request);

            final List<SearchResult> unfilteredSearchResult = blogPostSearcher.getBlogPosts(
                    time, spaceKeys, labels, matchAllLabels, sort, sortAscending, maxResults
            );

            final int pageStart = (page - 1) * resultsPerPage;
            final int pageEnd = Math.min(pageStart + resultsPerPage, unfilteredSearchResult.size());


            if (pageStart >= 0 && pageEnd <= unfilteredSearchResult.size())
            {
                final List<SearchResult> filteredSearchResult = unfilteredSearchResult.subList(pageStart, pageEnd);
                final Set<String> columns = getColumns(request);
                final String dataInJson = getResultsAsJsonString(request, page, unfilteredSearchResult, filteredSearchResult, columns, compactMode);
                final Writer writer = response.getWriter();

                response.setContentType("application/json");
                
                writer.write(dataInJson);
            }
            else
            {
                throw new ServletException("Paged results out of range. Page start index: " + pageStart
                        + "; Page end index: "+ pageEnd
                        + "; Unfiltered result size: " + unfilteredSearchResult.size());
            }
        }
        catch (final Exception e)
        {
            logger.error("Unable to return blog posts in JSON.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("text/plain");
            response.getWriter().write("Failed to get blog posts for URL: " + request.getRequestURL() + request.getQueryString() + "\n" + ExceptionUtils.getFullStackTrace(e));
        }
    }

    private String getTime(final HttpServletRequest request)
    {
        return StringUtils.defaultString(request.getParameter("time"));
    }

    private int getMaxResults(final HttpServletRequest request)
    {
        return Integer.parseInt(request.getParameter("maxResults"));
    }

    private Set<String> getSpaceKeys(final HttpServletRequest request)
    {
        return new HashSet<String>(
            Arrays.asList(
                    StringUtils.split(
                            StringUtils.defaultString(
                                    request.getParameter("spaceKeys")
                            ),
                            ","
                    )
            )
        );
    }

    private Set<String> getLabels(final HttpServletRequest request)
    {
        return new HashSet<String>(
            Arrays.asList(
                    StringUtils.split(
                            StringUtils.defaultString(
                                    request.getParameter("labels")
                            ),
                            ","
                    )
            )
        );
    }

    private boolean isMatchAllLabels(final HttpServletRequest request)
    {
        return Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("matchAllLabels")));
    }

    private int getPage(final HttpServletRequest request)
    {
        final String pageString = request.getParameter("page");
        return StringUtils.isNotBlank(pageString) ? Integer.parseInt(StringUtils.trim(pageString)) : 1;
    }

    private int getResultsPerPage(final HttpServletRequest request)
    {
        final String resultsPerPageString = request.getParameter("rp");

        if (StringUtils.isNotBlank(resultsPerPageString))
        {
            return Integer.parseInt(resultsPerPageString);
        }
        else
        {
            return BlogPostSearcher.DEFAULT_MAX_RESULT;
        }
    }

    private BlogPostSearcher.Sort getSort(final HttpServletRequest request)
    {
        final String sortName = request.getParameter("sortname");

        if (StringUtils.equals("title", sortName))
            return BlogPostSearcher.Sort.TITLE;
        else if (StringUtils.equals("author", sortName))
            return BlogPostSearcher.Sort.AUTHOR;
        else if (StringUtils.equals("labels", sortName))
            return BlogPostSearcher.Sort.LABELS;
        else
            return BlogPostSearcher.Sort.DATE_POSTED;
    }

    private boolean isSortAscending(final HttpServletRequest request)
    {
        final String sortOrder = request.getParameter("sortorder");
        return !StringUtils.equalsIgnoreCase("desc", sortOrder);
    }

    private Set<String> getColumns(final HttpServletRequest request)
    {
        final Set<String> columns = new LinkedHashSet<String>(
                Arrays.asList(
                        StringUtils.split(
                                StringUtils.defaultString(request.getParameter("columns")),
                                ","
                        )
                )
        );

        if (columns.isEmpty())
            columns.addAll(ALL_COLUMNS);
        else
            columns.retainAll(ALL_COLUMNS);

        return columns;
    }

    private boolean isCompactMode(final HttpServletRequest request)
    {
        return BooleanUtils.toBoolean(StringUtils.defaultString(request.getParameter("compactMode")));
    }

    private String getResultsAsJsonString(
            final HttpServletRequest httpServletRequest,
            final int page,
            final List<SearchResult> unfilteredSearchResults,
            final List<SearchResult> filteredSearchResults,
            final Set<String> columns,
            final boolean compactMode) throws Exception
    {
        String contextPath = httpServletRequest.getContextPath();
        JsonHelper jsonHelper = new JsonHelper();

        JSONObject fgResult = new JSONObject();
        fgResult.put("page", page);
        fgResult.put("total", unfilteredSearchResults.size());

        JSONArray fgResultArray = new JSONArray();
        for (SearchResult filteredResult : filteredSearchResults)
        {
            JSONObject fgResultElem = new JSONObject();
            fgResultElem.put("id", filteredResult.getHandle().toString());

            JSONArray fgResultElemCellArray = new JSONArray();
            for (String column : columns)
            {
                if (StringUtils.equals(column, "title"))
                    fgResultElemCellArray.put(jsonHelper.getContentLink(contextPath, filteredResult, compactMode));

                if (StringUtils.equals(column, "author"))
                    fgResultElemCellArray.put(jsonHelper.getAuthorSpan(filteredResult));

                if (StringUtils.equals(column, "labels"))
                    fgResultElemCellArray.put(jsonHelper.getLabelLinks(contextPath, filteredResult));

                if (StringUtils.equals(column, "date-posted"))
                    fgResultElemCellArray.put(jsonHelper.getPostedDateSpan(filteredResult));
            }
            fgResultElem.put("cell", fgResultElemCellArray);
            fgResultArray.put(fgResultElem);
        }
        
        fgResult.put("rows", fgResultArray);
        return fgResult.toString();
    }

    public class JsonHelper
    {
        public String getAuthorSpan(SearchResult searchResult)
        {
            User user = null != searchResult.getCreator() ? userAccessor.getUser(searchResult.getCreator()) : null;

            return new StringBuilder("<span class='flexi-blog-posts-author-link'>")
                    .append(null != user ? GeneralUtil.htmlEncode(user.getFullName()) : UserAccessor.ANONYMOUS)
                    .append("</span>")
                    .toString();
        }

        public String getLabelLinks(String contextPath, SearchResult searchResult)
        {
            StringBuilder labelLinksBuilder = new StringBuilder();
            Collection<String> labels = searchResult.getLabels(AuthenticatedUserThreadLocal.getUser());

            if (null != labels && !labels.isEmpty())
            {
                for (String label : labels)
                {
                    if (labelLinksBuilder.length() > 0)
                        labelLinksBuilder.append(" ");
                    labelLinksBuilder.append("<a class='flexi-blog-posts-label-link' ")
                            .append(" href='").append(contextPath).append("/label/").append(searchResult.getSpaceKey()).append('/').append(GeneralUtil.urlEncode(label)).append("'>")
                            .append(GeneralUtil.htmlEncode(label))
                            .append("</a>");
                }
            }

            return labelLinksBuilder.toString();
        }

        public String getPostedDateSpan(SearchResult searchResult)
        {
            return new StringBuilder("<span class='flexi-blog-posts-date-posted'>").append(getDateFormatter().format(searchResult.getLastModificationDate())).append("</span>").toString();
        }

        public String getContentLink(String contextPath, SearchResult searchResult, boolean compactMode)
        {
            StringBuilder contentLinkBuilder = new StringBuilder("<a class='flexi-blog-posts-content-link' ")
                    .append("href='").append(contextPath).append(searchResult.getUrlPath()).append("'>")
                    .append(GeneralUtil.htmlEncode(searchResult.getDisplayTitle()))
                    .append("</a>");

            if (compactMode)
            {
                contentLinkBuilder.append("&nbsp;<small class='flexi-blog-posts-compacted-author-and-date-posted'>")
                        .append(getI18nBean().getText("flexi-blog-posts.by-author-on", Arrays.asList( getAuthorSpan(searchResult), getPostedDateSpan(searchResult) )))
                        .append("</small>&nbsp;<small>")
                        .append(getLabelLinks(contextPath, searchResult)).append("</small>");
            }

            return contentLinkBuilder.toString();
        }
    }

}
