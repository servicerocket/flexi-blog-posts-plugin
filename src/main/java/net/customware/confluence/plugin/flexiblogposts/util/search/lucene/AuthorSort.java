package net.customware.confluence.plugin.flexiblogposts.util.search.lucene;

import com.atlassian.confluence.search.v2.sort.AbstractSort;

public class AuthorSort extends AbstractSort
{
    private static final String KEY = "creatorName";

    public AuthorSort(Order order)
    {
        super(KEY, order);
    }
}
