package net.customware.confluence.plugin.flexiblogposts.util.search;

import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchResult;

import java.util.List;
import java.util.Set;


public interface BlogPostSearcher
{
    String ALL_SPACES = "@all";

    int DEFAULT_MAX_RESULT = 15;

    public enum Sort
    {
        TITLE,
        AUTHOR,
        DATE_POSTED,
        LABELS
    }
    
    List<SearchResult> getBlogPosts(
            final String timespan,
            final Set<String> spaceKeys,
            final Set<String> labels,
            final boolean matchAllLabels,
            final Sort sort,
            final boolean sortAscending,
            final int maxResult) throws InvalidSearchException;
}
