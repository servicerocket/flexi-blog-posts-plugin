package net.customware.confluence.plugin.flexiblogposts.util.search.lucene;

import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.lucene.LuceneSortMapper;
import org.apache.lucene.search.Sort;

public class BlogPostsSortMapper implements LuceneSortMapper
{
    public Sort convertToLuceneSort(SearchSort searchSort)
    {
        return new Sort(searchSort.getKey(), searchSort.getOrder() == SearchSort.Order.DESCENDING);
    }
}
