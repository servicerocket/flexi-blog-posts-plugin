package net.customware.confluence.plugin.flexiblogposts.util.search.lucene;

import com.atlassian.confluence.search.v2.sort.AbstractSort;

public class LabelSort extends AbstractSort
{
    private static final String KEY = "labelText";

    public LabelSort(Order order)
    {
        super(KEY, order);
    }
}
