package net.customware.confluence.plugin.flexiblogposts.util.search;

import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.searchfilter.ContentPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.AbstractSort;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.search.v2.sort.TitleSort;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.util.profiling.UtilTimerStack;
import net.customware.confluence.plugin.flexiblogposts.util.search.lucene.AuthorSort;
import net.customware.confluence.plugin.flexiblogposts.util.search.lucene.LabelSort;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DefaultBlogPostSearcher implements BlogPostSearcher
{
    private static final Logger logger = LoggerFactory.getLogger(DefaultBlogPostSearcher.class);

    private final SearchManager searchManager;

    public DefaultBlogPostSearcher(SearchManager searchManager)
    {
        this.searchManager = searchManager;
    }

    public List<SearchResult> getBlogPosts(
            final String timespan,
            final Set<String> spaceKeys,
            final Set<String> labels,
            final boolean matchAllLabels,
            final Sort sort,
            final boolean sortAscending,
            final int maxResult) throws InvalidSearchException
    {
        final String stackFrameName = "DefaultBlogPostSearcher.getBlogPosts(String, Set<String>, Set<String>, boolean, Sort, boolean, int):SearchResults";

        UtilTimerStack.push(stackFrameName);

        try
        {
            final Date fromDate = getDateBeforeToday(convertDurationToSeconds(timespan));
            final Set<String> rewrittenSpaceKeys = null == spaceKeys || spaceKeys.isEmpty()
                    ? new HashSet<String>(Arrays.asList(ALL_SPACES))
                    : spaceKeys;

            return searchManager.search(
                    new ContentSearch(
                            constructSearchQuery(
                                    fromDate,
                                    rewrittenSpaceKeys,
                                    null == labels ? new HashSet<String>(0) : labels,
                                    matchAllLabels
                            ),
                            getSort(sort, !sortAscending),
                            ContentPermissionsSearchFilter.getInstance(),
                            new SubsetResultFilter(0, maxResult <= 0 ? DEFAULT_MAX_RESULT : maxResult)
                    )
            ).getAll();
        }
        finally
        {
            UtilTimerStack.pop(stackFrameName);
        }
    }

    private AbstractSort getSort(Sort s, boolean reverse)
    {
        SearchSort.Order searchSort = reverse ? SearchSort.Order.DESCENDING : SearchSort.Order.ASCENDING;
        if (s == Sort.AUTHOR)
            return new AuthorSort(searchSort);
        else if (s == Sort.LABELS)
            return new LabelSort(searchSort);
        else if (s == Sort.TITLE)
            return new TitleSort(searchSort);
        else
            return new CreatedSort(searchSort);
    }

    private int convertDurationToSeconds(final String durationString)
    {
        if (StringUtils.isNotBlank(durationString))
        {
            try
            {
                return (int) DateUtils.getDuration(durationString);
            }
            catch (final InvalidDurationException ide)
            {
                logger.warn("Invalid duration specified: " + durationString, ide);
                return (int) ((System.currentTimeMillis() - getLastYearDate()) / 1000L);
            }
        }
        else
        {
            return (int) ((System.currentTimeMillis() - getLastYearDate()) / 1000L);
        }
    }

    private long getLastYearDate()
    {
        final Calendar now = Calendar.getInstance();

        now.add(Calendar.YEAR, -1);
        return now.getTimeInMillis();
    }

    private Date getDateBeforeToday(final int durationSecondsSince)
    {
        Calendar now = Calendar.getInstance();

        now.add(Calendar.SECOND, -durationSecondsSince);
        return now.getTime();
    }

    private SearchQuery constructLastModifiedDateRangeQuery(final Date fromDate)
    {
        return new DateRangeQuery(
                fromDate,
                new Date(),
                true, true,
                DateRangeQuery.DateRangeQueryType.MODIFIED);
    }

    private SearchQuery constructLabelQuery(final Set<String> labels, final boolean matchAllLabels)
    {
        final Set<LabelQuery> labelQueries = new HashSet<LabelQuery>();

        for (final String label : labels)
            labelQueries.add(new LabelQuery(label));

        return matchAllLabels
                ? BooleanQuery.composeAndQuery(labelQueries)
                : BooleanQuery.composeOrQuery(labelQueries);
    }

    private SearchQuery constructInSpaceQuery(final Set<String> spaceKeys)
    {
        final Set<InSpaceQuery> inSpaceQueries = new HashSet<InSpaceQuery>();

        if (!spaceKeys.contains(ALL_SPACES))
        {
            for (final String spaceKey: spaceKeys)
                inSpaceQueries.add(new InSpaceQuery(spaceKey));

            return BooleanQuery.composeOrQuery(inSpaceQueries);
        }

        return null;
    }

    private SearchQuery constructContentTypeQuery()
    {
        return new ContentTypeQuery(ContentTypeEnum.BLOG);
    }

    protected SearchQuery constructSearchQuery(
            final Date fromDate,
            final Set<String> spaceKeys,
            final Set<String> labelString,
            final boolean matchAllLabels)
    {
        final SearchQuery modifiedDateQuery = constructLastModifiedDateRangeQuery(fromDate);
        final SearchQuery spaceQuery = constructInSpaceQuery(spaceKeys);
        final SearchQuery labelQuery = constructLabelQuery(labelString, matchAllLabels);
        final SearchQuery contentTypeQuery = constructContentTypeQuery();
        final Set<SearchQuery> searchQueries = new HashSet<SearchQuery>(Arrays.asList(modifiedDateQuery, labelQuery, contentTypeQuery));

        if (null != spaceQuery)
            searchQueries.add(spaceQuery);

        return BooleanQuery.composeAndQuery(searchQueries);
    }
}
