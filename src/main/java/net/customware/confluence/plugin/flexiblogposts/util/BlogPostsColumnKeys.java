package net.customware.confluence.plugin.flexiblogposts.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public interface BlogPostsColumnKeys
{
    String COLUMN_TITLE = "title";

    String COLUMN_AUTHOR = "author";

    String COLUMN_DATE_POSTED = "date-posted";

    String COLUMN_LABELS = "labels";

    Set<String> ALL_COLUMNS = Collections.unmodifiableSet(
            new LinkedHashSet<String>(
                    Arrays.asList(
                            COLUMN_TITLE,
                            COLUMN_AUTHOR,
                            COLUMN_DATE_POSTED,
                            COLUMN_LABELS
                    )
            )
    );
}
