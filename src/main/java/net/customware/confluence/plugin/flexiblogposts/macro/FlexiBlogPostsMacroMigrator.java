package net.customware.confluence.plugin.flexiblogposts.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FlexiBlogPostsMacroMigrator implements MacroMigration
{
    private static final Logger LOG = LoggerFactory.getLogger(FlexiBlogPostsMacroMigrator.class);

    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        final String defaultParameter = macroDefinition.getDefaultParameterValue();
        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();

        if (StringUtils.isNotBlank(defaultParameter) && StringUtils.isNumeric(defaultParameter))
        {
            macroDefinition.setParameters(
                    new HashMap<String, String>(macroDefinition.getParameters())
                    {
                        {
                            put("maxResults", defaultParameter);
                        }
                    }
            );
        }

        if (!conversionErrors.isEmpty())
            for (RuntimeException conversionError : conversionErrors)
                LOG.error(String.format("Unable to convert default parameter to parameter named \"maxResults\" %s", defaultParameter), conversionError);

        return macroDefinition;
    }
}
